#ifndef OCC_H
#define OCC_H

#include <errno.h>
#include <sched.h>
#include <stdatomic.h>
#include <stdbool.h>
#include <stdlib.h>
#include <pthread.h>

#if ATOMIC_LLONG_LOCK_FREE >= 2
typedef unsigned long long occ_version_t;
#elif ATOMIC_LONG_LOCK_FREE >= 2
typedef unsigned long occ_version_t;
#elif ATOMIC_INT_LOCK_FREE >= 2
typedef unsigned int occ_version_t;
#else
#error No suitable lock-free atomic type.
#endif

typedef struct
{
    pthread_rwlock_t lock;
    _Atomic occ_version_t version;
} occ_t;

int occ_init(occ_t*);
int occ_destroy(occ_t*);

inline occ_version_t occ_getversion(const occ_t*);
inline int occ_earlycheck(occ_version_t);
inline int occ_checkversion(const occ_t*, occ_version_t);
inline int occ_checkversion_fast(const occ_t*, occ_version_t);
inline int occ_rdlock(occ_t*);
inline int occ_tryrdlock(occ_t*);
inline int occ_rdunlock(occ_t*);

#define OCC_RDTXN(occ) \
    for (struct occ_rdtxn_state occ_state = { .occ = occ; .step = 0 }; occ_rdtxn_step(&occ_state); )

#define OCC_CHECKPOINT \
    if (occ_state.step < 2 && occ_checkversion_fast(occ_state.occ, occ_state.version) != 0) continue; else (void) 0

int occ_wrlock(occ_t*);
int occ_trywrlock(occ_t*);
int occ_wrunlock(occ_t*);

//
// INTERNAL
//

inline occ_version_t occ_getversion(const occ_t* const occ)
{
    return atomic_load_explicit(&occ->version, memory_order_acquire);
}

inline int occ_earlycheck(const occ_version_t version)
{
    return (version & 1u) != 0u;
}

inline int occ_checkversion_fast(const occ_t* const occ, const occ_version_t version)
{
    atomic_thread_fence(memory_order_acquire);
    return atomic_load_explicit(&occ->version, memory_order_relaxed) != version;
}

inline int occ_checkversion(const occ_t* const occ, const occ_version_t version)
{
    return occ_checkversion_fast(occ, version) || occ_earlycheck(version);
}

inline int occ_rdlock(occ_t* const occ)
{
    return pthread_rwlock_rdlock(&occ->lock);
}

inline int occ_tryrdlock(occ_t* const occ)
{
    return pthread_rwlock_tryrdlock(&occ->lock);
}

inline int occ_rdunlock(occ_t* const occ)
{
    if ((atomic_load_explicit(&occ->version, memory_order_relaxed) & 1u) != 0u) return EPERM;
    return pthread_rwlock_unlock(&occ->lock);
}

struct occ_rdtxn_state
{
    occ_t* occ;
    int step;
    occ_version_t version;
};

inline bool occ_rdtxn_step(struct occ_rdtxn_state* const state)
{
    switch (state->step)
    {
    case 0:
        ++state->step;
        // fallthrough to do_load

    do_load:
    {
        const occ_version_t version = occ_getversion(state->occ);
        if (occ_earlycheck(version) != 0) goto do_lock;
        state->version = version;
        return true;
    }

    case 1:
        if (occ_checkversion_fast(state->occ, state->version) == 0)
            return false;
        // fallthrough to do_lock

    do_lock:
    {
        const int err = occ_rdlock(state->occ);
        if (err == EAGAIN)
        {
            sched_yield();
            goto do_load;
        }
        else if (err != 0)
            _Exit(EXIT_FAILURE);
        else
            ++state->step;
        return true;
    }

    case 2:
        if (occ_rdunlock(state->occ) != 0) _Exit(EXIT_FAILURE);
        return false;

    default:
        __builtin_unreachable();
    }
}

#endif
