#ifndef CTP_OCCPP_H
#define CTP_OCCPP_H

#include <atomic>
#include <shared_mutex>
#include <type_traits>

namespace ctp
{

enum class occ_result
{
    consistent = 0,
    not_consistent = 1
};

class occ
{
public:
#if ATOMIC_LLONG_LOCK_FREE >= 2
    using version = unsigned long long;
#elif ATOMIC_LONG_LOCK_FREE >= 2
    using version = unsigned long;
#elif ATOMIC_INT_LOCK_FREE >= 2
    using version = unsigned int;
#else
#error No suitable lock-free atomic type.
#endif

    occ();

    inline version get_version() const noexcept;
    static inline occ_result early_check(version) noexcept;
    inline occ_result check_version(version) const noexcept;
    inline occ_result check_version_fast(version) const noexcept;

    inline void lock_shared();
    inline bool try_lock_shared();
    inline void unlock_shared();

    void lock();
    bool try_lock();
    void unlock();

private:
    std::shared_mutex m_lock;
    std::atomic<version> m_version;
};

template<typename F> inline std::invoke_result_t<F> occ_read_transaction(occ&, F);

class occ_checkpointer {
public:
    inline void operator()() const;

    occ_checkpointer() noexcept = default;
    inline occ_checkpointer(const occ*, occ::version) noexcept;

private:
    const occ* m_occ = nullptr;
    occ::version m_version;
};

template<typename F> inline std::invoke_result_t<F, occ_checkpointer> occ_read_transaction(occ&, F);


//
// INTERNAL
//

inline occ::version occ::get_version() const noexcept
{
    return m_version.load(std::memory_order_acquire);
}

inline occ_result occ::early_check(const occ::version version) noexcept
{
    return static_cast<occ_result>(version & 1u);
}

inline occ_result occ::check_version(const occ::version version) const noexcept
{
    return static_cast<occ_result>(static_cast<int>(check_version_fast(version)) || static_cast<int>(early_check(version)));
}

inline occ_result occ::check_version_fast(const occ::version version) const noexcept
{
    std::atomic_thread_fence(std::memory_order_acquire);
    return static_cast<occ_result>(m_version.load(std::memory_order_relaxed) != version);
}

inline void occ::lock_shared()
{
    m_lock.lock_shared();
}

inline bool occ::try_lock_shared()
{
    return m_lock.try_lock_shared();
}

inline void occ::unlock_shared()
{
    m_lock.unlock_shared();
}

template<typename F> inline std::invoke_result_t<F> occ_read_transaction(occ& occ_, F f)
{
    const occ::version version = occ_.get_version();
    if (occ::early_check(version) == occ_result::consistent)
    {
        try
        {
            auto ret = f();
            if (occ_.check_version_fast(version) == occ_result::consistent)
                return ret;
        }
        catch (...)
        {
            if (occ_.check_version_fast(version) == occ_result::consistent)
                throw;
        }
    }

    std::shared_lock<occ> l(occ_);
    return f();
}

class occ_fast_fail { };

inline void occ_checkpointer::operator()() const
{
    if (m_occ && m_occ->check_version_fast(m_version) != occ_result::consistent)
        throw occ_fast_fail{};
}

inline occ_checkpointer::occ_checkpointer(const occ* const occ_, const occ::version version) noexcept
  : m_occ{occ_}, m_version{version}
{ }

template<typename F> inline std::invoke_result_t<F, occ_checkpointer> occ_read_transaction(occ& occ_, F f)
{
    const occ::version version = occ_.get_version();
    if (occ::early_check(version) == occ_result::consistent)
    {
        try
        {
            auto ret = f(occ_checkpointer(&occ_, version));
            if (occ_.check_version_fast(version) == occ_result::consistent)
                return ret;
        }
        catch (occ_fast_fail)
        { }
        catch (...)
        {
            if (occ_.check_version_fast(version) == occ_result::consistent)
                throw;
        }
    }

    std::shared_lock<occ> l(occ_);
    return f(occ_checkpointer());
}

}  // namespace ctp

#endif
