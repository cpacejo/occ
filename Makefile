CFLAGS = -std=c11 -pthread -Wall -Wextra -pedantic -Werror -O3
CXXFLAGS = -std=c++17 -Wall -Wextra -pedantic -Werror -O3
CPPFLAGS = -D_XOPEN_SOURCE=600

OBJS = occ.o occpp.o

.PHONY: all
all: $(OBJS)

occ.o: occ.h
occpp.o: occpp.h
