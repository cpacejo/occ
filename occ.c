#include <errno.h>
#include <stdatomic.h>
#include <stdbool.h>
#include <stdlib.h>
#include <pthread.h>
#include "occ.h"

int occ_init(occ_t* const occ)
{
    int err;

    pthread_rwlockattr_t lock_attr;
    err = pthread_rwlockattr_init(&lock_attr);
    if (err != 0) return err;
    err = pthread_rwlockattr_setkind_np(&lock_attr, PTHREAD_RWLOCK_PREFER_WRITER_NONRECURSIVE_NP);
    if (err != 0) return err;
    err = pthread_rwlock_init(&occ->lock, &lock_attr);
    if (pthread_rwlockattr_destroy(&lock_attr) != 0) _Exit(EXIT_FAILURE);
    if (err != 0) return err;

    atomic_init(&occ->version, 0u);

    return err;
}

int occ_destroy(occ_t* const occ)
{
    return pthread_rwlock_destroy(&occ->lock);
}

extern occ_version_t occ_getversion(const occ_t*);
extern int occ_earlycheck(occ_version_t);
extern int occ_checkversion(const occ_t*, occ_version_t);
extern int occ_rdlock(occ_t*);
extern int occ_tryrdlock(occ_t*);
extern int occ_unlock(occ_t*);

extern bool occ_rdtxn_check(struct occ_rdtxn_state*);

int occ_wrlock(occ_t* const occ)
{
    int err;

    err = pthread_rwlock_wrlock(&occ->lock);
    if (err != 0) return err;

    const occ_version_t version = atomic_load_explicit(&occ->version, memory_order_relaxed);
    if ((version & 1u) != 0u) _Exit(EXIT_FAILURE);

    atomic_store_explicit(&occ->version, version + 1u, memory_order_relaxed);

    atomic_thread_fence(memory_order_release);

    return 0;
}

int occ_trywrlock(occ_t* const occ)
{
    int err;

    err = pthread_rwlock_trywrlock(&occ->lock);
    if (err != 0) return err;

    const occ_version_t version = atomic_load_explicit(&occ->version, memory_order_relaxed);
    atomic_store_explicit(&occ->version, version + 1u, memory_order_relaxed);

    atomic_thread_fence(memory_order_release);

    return 0;
}

int occ_wrunlock(occ_t* const occ)
{
    const occ_version_t version = atomic_load_explicit(&occ->version, memory_order_relaxed);
    if ((version & 1u) != 1u) return EPERM;

    atomic_store_explicit(&occ->version, version + 1u, memory_order_release);

    return pthread_rwlock_unlock(&occ->lock);
}
