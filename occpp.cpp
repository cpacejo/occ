#include <atomic>
#include <exception>
#include <shared_mutex>
#include "occpp.h"

namespace ctp
{

occ::occ()
    : m_version(0u)
{ }

void occ::lock()
{
    m_lock.lock();

    const occ::version version = m_version.load(std::memory_order_relaxed);
    if ((version & 1u) != 0u) std::terminate();

    m_version.store(version + 1u, std::memory_order_relaxed);

    std::atomic_thread_fence(std::memory_order_release);
}

bool occ::try_lock()
{
    if (!m_lock.try_lock())
        return false;

    const occ::version version = m_version.load(std::memory_order_relaxed);
    if ((version & 1u) != 0u) std::terminate();

    m_version.store(version + 1u, std::memory_order_relaxed);

    std::atomic_thread_fence(std::memory_order_release);

    return true;
}

void occ::unlock()
{
    const occ::version version = m_version.load(std::memory_order_relaxed);

    m_version.store(version + 1u, std::memory_order_release);

    m_lock.unlock();
}

}  // namespace ctp
